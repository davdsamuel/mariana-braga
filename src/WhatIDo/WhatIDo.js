import React from "react";
import styled from 'styled-components'
import Project from './../Components/Project/Project';
import BackButton from './../Components/BackButton/BackButton';

import bo from './../bo.json';

const Wrapper = styled.main`
    display: flex;
    justify-content: start;
    height: 100vh;
    align-items: center;
    max-width: 100vw;
    overflow-x: hidden;
    > img {
        left: 50px;
        top: 30vh;
        @media screen and (max-width: 1200px) {
            top: 19px;
            left: 20px;
        }
    }
`;
const List = styled.ul`
    list-style: none;
    margin: 0;
    display: flex;
    justify-content: end;
    align-items: center;
    flex-wrap: wrap;
    width: 88%;
    &> li:last-child::after {
        display: none;
    }
`;

class WhatIDo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          animation: 'fadeInUp',   
        };
        this.goTo = this.goTo.bind(this);
    }

    goTo = (url) => {
        this.setState({
            animation: 'fadeOut',
        });
        setTimeout(() => {
            this.props.history.push(url);
        }, 2000);
    }

    applyToBig = (id, url) => {
       
    }
      
    makeHover = (w, n) => {
        const childrenCount = document.getElementById("list").childElementCount;
        if(w === 0) {
            for (let index = 0; index < childrenCount; index++) {
                if(index !== n) {
                    document.getElementById("list").children[index].style.visibility = 'hidden';
                }
            }
        } else {
            for (let index = 0; index < childrenCount; index++) {
                document.getElementById("list").children[index].style.visibility = 'initial';
            }
        }
    
    }

    handleLeave = (url) => {
        //
        this.setState({
            animation: 'fadeOutDown',
        });
        setTimeout(() => {
            this.props.history.push(url)
        }, 2000)
    }

    render() {

        const Projects = bo.projects;

        const listProjects = Projects.map(project => <Project applyToBig={this.applyToBig} hover={this.makeHover} key={project.id} animation={this.state.animation} project={project} />);

        return (
            <Wrapper>
                <BackButton animation={this.state.animation} url='/' onClick={this.handleLeave} top='38vh' />
                <List id="list">
                    { listProjects }
                </List>
            </Wrapper>
        );
    }
}

export default WhatIDo;