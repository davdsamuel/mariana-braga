import React from 'react';
import styled from 'styled-components';
import Arrow from './back-arrow.png';

const Back = styled.img`
    width: 56px;
    position: absolute;
    filter: contrast(0);
    top: ${props => props.top};
    cursor: pointer;
    animation-delay: 1s;
    transition: all ease 0.2s;
    z-index: 10;
    &:hover {
        margin-left: -9px;
    }
`;

const BackButton = (props) => <Back id="backButton" onClick={() => props.onClick(props.url)} className={`animated ${props.animation}`} top={props.top} alt="back-button" src={Arrow} />;

export default BackButton;