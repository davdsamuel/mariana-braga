import React from "react";
import styled from "styled-components";

const ListItem = styled.li`
  margin: 15px 1px;
  @media screen and (max-width: 992px) {
    margin: 4px 1px;
    width: 70%;
  }
  position: relative;
  padding: 0 90px 0 0;
  &:hover {
    height: 20vh;
  }
  &:hover > div {
    display: block;
    top: 74px;
  }
  &:hover > div > p {
    height: auto;
    width: 100%;
    max-width: 600px;
  }
  &:hover hr {
    width: 100%;
    margin: -11px 0 0 0;
    height: 19px;
  }
  &::after {
    content: " ";
    position: absolute;
    right: 13px;
    top: 21px;
    width: 59px;
    height: 5px;
    background: #969696;
    z-index: -1;
  }
`;
const ListItemButton = styled.button`
  background: none;
  border: 0;
  outline: none;
  font-family: "Source Sans Pro", sans-serif;
  padding: 0;
  letter-spacing: 4px;
  color: #969696;
  font-weight: 800;
  font-size: 35px;
  @media screen and (max-width: 992px) {
    font-size: 19px;
    text-align: left;
  }
  text-transform: lowercase;
  display: block;
  width: 100%;
`;
const Content = styled.div`
  position: absolute;
  top: 100px;
  left: 0;
  display: none;
  transition: all ease 0.2s;
`;
const Subtext = styled.p`
  margin: 0;
  height: 0;
  overflow: hidden;
`;

const Subtitle = styled.p`
  font-size: 16px;
  font-weight: 800;
  text-transform: uppercase;
  margin: 30px 0 18px 0;
  letter-spacing: 2px;
`;

const ColorsSquares = styled.div`
  > a {
    display: inline-block;
    margin: 0 25px 25px 0;
    position: relative;
    z-index: 10;
  }
  > a > img {
    position: absolute;
    display: none;
  }
  > a:hover > img {
    display: block;
  }
`;
const Color = styled.div`
  text-decoration: none;
  width: 30px;
  height: 30px;
  display: inline-block;
  padding: 0 0 0 0px;
  margin: 0 10px 0 0;
`;

const HR = styled.hr`
  margin: 10px 0 0 0;
  border: 0;
  width: 0;
  height: 8px;
  transition: all 0.6s cubic-bezier(0.785, 0.135, 0.15, 0.86);
`;

const Posts = styled.div`
  display: flex;
  justify-content: end;
  align-items: center;
  width: 88vw;
  flex-wrap: wrap;
`;
const Post = styled.div`
  padding: 0 2% 30px 0;
  text-align: center;
  display: inline-block;
  min-width: 150px;
  img {
    width: 80px;
  }
  &:hover > img {
    transform: scale(4);
    top: -15vh;
    position: relative;
  }
  a {
    background: #f5f5f5;
    padding: 10px 15px;
    color: #212121;
    cursor: pointer;
    margin: 11px 0 0 0;
    display: inline-block;
    border-radius: 3px;
    font-weight: 100;
    text-decoration: none;
    width: calc(100% - 20px);
    cursor: pointer;
    & i {
      padding: 0 10px 0 0;
    }
  }
`;

const SeeMore = styled.a`
  width: 30%;
  padding: 0;
  color: grey;
  text-decoration: none;
  text-transform: uppercase;
  font-weight: 700;
  letter-spacing: 1px;
  hr {
    height: 22px !important;
    border: 0;
    width: 0% !important;
    margin: -9px 0 0 0;
    transition: all 0.6s cubic-bezier(0.785, 0.135, 0.15, 0.86) !important;
  }
  &:hover hr {
    width: 100% !important;
  }
`;

const Project = (props) => {
  let posts;
  if (props.project.type === "posts") {
    posts = props.project.posts.map((post) => (
      <Post key={post.id}>
        <a
          href={
            post.social === "facebook"
              ? `${post.url}`
              : `https://www.instagram.com/${
                  post.tag && post.tag.replace("@", "")
                }`
          }
          rel="noopener noreferrer"
          target="_blank"
        >
          {post.social === "facebook" && <i className="fab fa-facebook-f"></i>}
          {post.social === "instagram" && <i className="fab fa-instagram"></i>}
          {post.tag}
        </a>
      </Post>
    ));
  }
  if (props.project.type === "articles") {
    posts = props.project.posts.map((post) => (
      <a
        key={post.id}
        target="_blank"
        rel="noopener noreferrer"
        href={post.link}
        onMouseOver={() => props.applyToBig(props.project.bigImage, post.img)}
      >
        <Color style={{ backgroundColor: props.project.color }}></Color>
      </a>
    ));
  }
  if (props.project.type === "websites") {
    posts = props.project.posts.map((post) => (
      <Post key={post.id}>
        <a href={post.link} rel="noopener noreferrer" target="_blank">
          {post.title}
        </a>
      </Post>
    ));
  }
  return (
    <ListItem
      onMouseOver={() => props.hover(0, props.project.id)}
      onMouseLeave={() => props.hover(1, 0)}
      className={`animated ${props.animation}`}
    >
      <ListItemButton>{props.project.title}</ListItemButton>
      <HR style={{ background: props.project.color }} />
      <Content>
        <Subtext>{props.project.subtext}</Subtext>
        <Subtitle>{props.project.Subtitle}</Subtitle>
        {props.project.type === "posts" || props.project.type === "websites" ? (
          <Posts>{posts}</Posts>
        ) : null}
        {props.project.type === "articles" && (
          <ColorsSquares>
            {posts}
            {props.project.seeMore && (
              <SeeMore href={props.project.seeMore} target="_blank">
                See more
                <hr style={{ background: props.project.color }} />
              </SeeMore>
            )}
          </ColorsSquares>
        )}
      </Content>
    </ListItem>
  );
};

export default Project;
