import React from "react";
import styled from "styled-components";
import BackButton from "./../Components/BackButton/BackButton";
import bo from "./../bo.json";

const Wrapper = styled.main`
  padding: 40px;
  margin: 8vh 0 0;
`;

const CenterDiv = styled.section`
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 34vw;
  max-height: 44vh;
  @media screen and (max-height: 780px) and (min-width: 850px) {
    height: 24vw;
  }
  @media screen and (max-height: 780px) and (min-width: 850px) {
    display: none;
  }
  @media screen and (max-width: 992px) {
    height: auto;
    max-height: auto;
  }
`;
const Text = styled.article`
  width: 47%;
  min-width: 500px;
  font-family: "Reenie Beanie", cursive;
  font-size: 30px;
  color: #969696;
  @media screen and (max-width: 992px) {
    font-size: 22px;
    color: #969696;
    width: 100%;
    min-width: auto;
  }
`;

const HilightSkills = styled.section`
  padding: 85px 0 0;
  text-align: center;
`;
const Subtitle = styled.h2`
  z-index: 1;
  color: #969696;
  font-weight: 800;
  font-size: 30px;
  @media screen and (max-width: 992px) {
    font-size: 23px;
  }
  position: relative;
  text-align: center;
  text-transform: lowercase;
  display: block;
  width: 100%;
  span {
    position: relative;
    z-index: 1;
  }
  hr {
    width: 286px;
    height: 20px;
    border: 0;
    background: #e3f2fd;
    position: absolute;
    top: 14px;
    right: 0;
    left: 0;
    @media screen and (max-width: 992px) {
      width: 216px;
      height: 20px;
      top: 4px;
    }
  }
`;

const Skills = styled.article`
  padding: 50px 18%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  @media screen and (max-width: 992px) {
    flex-wrap: wrap;
  }
  > span:nth-child(0) {
    animation-delay: 1.1s;
  }
  > span:nth-child(1) {
    animation-delay: 1.2s;
  }
  > span:nth-child(2) {
    animation-delay: 1.3s;
  }
  > span:nth-child(3) {
    animation-delay: 1.4s;
  }
  > span:nth-child(4) {
    animation-delay: 1.5s;
  }
  > span:nth-child(5) {
    animation-delay: 1.6s;
  }
  > span:nth-child(6) {
    animation-delay: 1.7s;
  }
  > span:nth-child(7) {
    animation-delay: 1.8s;
  }
  > span:nth-child(8) {
    animation-delay: 1.9s;
  }
`;
const Skill = styled.span`
  cursor: pointer;
  font-size: 26px;
  p {
    color: #000000;
    font-weight: 400;
    font-size: 27px;
    font-family: "Reenie Beanie", cursive;
  }
  &:hover p {
    opacity: 1;
  }
`;

const MainSkill = styled.p`
  font-size: 20px;
  text-transform: lowercase;
  font-weight: 500;
  transition: all ease 0.2s;
  opacity: 0;
`;

const B = styled.div`
  margin: 0;
  @media screen and (max-width: 992px) {
    position: absolute;
    top: 30px;
  }
`;

class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animation: "fadeInUp",
    };
    this.goTo = this.goTo.bind(this);
  }

  goTo = (url) => {
    this.setState({
      animation: "fadeOut",
    });
    setTimeout(() => {
      this.props.history.push(url);
    }, 2000);
  };

  render() {
    return (
      <Wrapper>
        <B>
          <BackButton
            animation={this.state.animation}
            onClick={() => this.goTo("/")}
          />
        </B>
        <CenterDiv className={`animated ${this.state.animation}`}>
          <Text>{bo.about.text}</Text>
        </CenterDiv>
        <HilightSkills>
          <Subtitle className={`animated ${this.state.animation}`}>
            <span> {bo.about.subtitle}</span>
            <hr className={`animated slideInLeft`} />
          </Subtitle>
          <Skills>
            {bo.about.skills.map(item => (
              <Skill className={`animated ${this.state.animation}`}>
                <span role="img" aria-label={item.name}>
                  {item.icon}
                </span>
                <MainSkill>{item.name}</MainSkill>
              </Skill>
            ))}
          </Skills>
        </HilightSkills>
      </Wrapper>
    );
  }
}

export default About;
