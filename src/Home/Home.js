import React from "react";
import styled from "styled-components";
import bo from "./../bo.json";

const Wrapper = styled.main`
  display: inline-block;
  width: 100%;
`;
const Banner = styled.section`
  width: 100%;
  margin: 17vh 0 0;
  @media screen and (max-width: 992px) {
    margin: 0 0 0;
  }
  height: 64vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Intro = styled.article`
  display: inline-block;
  width: 100%;
  text-align: center;
  > div {
    animation-duration: 0.4s;
  }
`;

const Title = styled.h1`
  margin: 0;
  display: block;
  font-family: "Source Sans Pro", sans-serif;
  font-size: 50px;
  letter-spacing: 2px;
  animation-duration: 1s;
  font-weight: 100;
`;

const Subtitle = styled.h3`
  margin: 0;
  display: block;
  font-family: "Reenie Beanie", cursive;
  font-size: 36px;
  color: #c7c7c7;
  margin: 25px 0;
  font-weight: 100;
  letter-spacing: 5px;
  animation-delay: 400ms;
  animation-duration: 1s;
`;
const Menu = styled.div`
  text-align: center;
  padding: 0 0 0;
  > div:nth-child(1) > button {
    animation-delay: 800ms;
  }
  > div:nth-child(2) > button {
    animation-delay: 1000ms;
  }
  > div:nth-child(3) > button {
    animation-delay: 1200ms;
  }
`;

const MenuItem = styled.button`
  display: inline-block;
  outline: none;
  text-decoration: none;
  color: #a0a0a0;
  font-weight: 100;
  font-size: 24px;
  margin: 7px 0;
  text-transform: lowercase;
  background: none;
  border: 0;
  font-weight: 100;
  cursor: pointer;
  font-family: "Source Sans Pro", sans-serif;
`;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animation: "fadeInUp",
    };
    this.goTo = this.goTo.bind(this);
  }
  goTo = (url) => {
    this.setState({
      animation: "fadeOut",
    });
    setTimeout(() => {
      this.props.history.push(url);
    }, 2000);
  };
  render() {
    return (
      <Wrapper>
        <Banner>
          <Intro>
            <Title className={`animated ${this.state.animation}`}>
              {bo.site_title}
            </Title>
            <Subtitle className={`animated ${this.state.animation}`}>
              {bo.site_subtitle}
            </Subtitle>
          </Intro>
        </Banner>
        <Menu>
          <div>
            <MenuItem
              className={`animated ${this.state.animation}`}
              onClick={() => this.goTo("/about")}
            >
              {bo.pages.about}
            </MenuItem>
          </div>
          <div>
            <MenuItem
              className={`animated ${this.state.animation}`}
              onClick={() => this.goTo("/services")}
            >
              {bo.pages.projects}
            </MenuItem>
          </div>
          <div>
            <MenuItem
              className={`animated ${this.state.animation}`}
              onClick={() => this.goTo("/contact")}
            >
              {bo.pages.contacts}
            </MenuItem>
          </div>
        </Menu>
      </Wrapper>
    );
  }
}

export default Home;
