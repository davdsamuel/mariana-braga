import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import WhatIDo from './WhatIDo/WhatIDo';
import Home from './Home/Home';
import About from './About/About';
import Contact from './Contact/Contact';
import styled from 'styled-components'
import ig from './under-construction.gif';

const Mobile = styled.div`
    /*display: flex;
    width: 100%;
    height: 100vh;
    align-items: center;
    justify-content: center;
    background: #fccb4f;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 10;
    img {
      width: 130%;
    }*/
    display: none;
    /*@media screen and (min-width: 992px) {
      display: none;
    }*/
`;

export default function render() {
  return (
    <Router>
        <Mobile>
          <img src={ig} alt="under construction" />
        </Mobile>
        <Route exact path="/" component={Home} />
        <Route exact path="/services" component={WhatIDo} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />
    </Router>
  );
}