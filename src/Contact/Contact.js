import React from "react";
import styled from "styled-components";
import cv from "./cv.pdf";
import BackButton from "./../Components/BackButton/BackButton";
import bo from "./../bo.json";

const Wrapper = styled.main`
  display: flex;
  width: 100%;
  height: 100vh;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`;

const Title = styled.h1`
  color: #969696;
  font-weight: 800;
  font-size: 35px;
  @media screen and (max-width: 992px) {
    font-size: 25px;
  }
  position: relative;
  text-align: center;
  text-transform: lowercase;
  display: block;
  width: 100%;
  margin: 0 0 60px 0;
`;

const List = styled.ul`
  display: block;
  width: 100%;
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  > li:nth-child(1) {
    animation-delay: 0.4s;
  }
  > li:nth-child(2) {
    animation-delay: 0.6s;
  }
  > li:nth-child(3) {
    animation-delay: 0.8s;
  }
  > li:nth-child(4) {
    animation-delay: 1s;
  }
  > li:nth-child(5) {
    animation-delay: 1.2s;
  }
  > li:nth-child(6) {
    animation-delay: 1.4s;
  }
`;

const ListItem = styled.li`
  display: block;
  width: 100%;
  padding: 11px 0;
  font-size: 21px;
  i {
    padding: 0 30px 0 0;
  }
  > a {
    text-decoration: none;
    color: inherit;
  }
  > a > i:nth-child(2) {
    margin: 0;
    padding: 0;
    color: #a8cde8;
    -webkit-transition: all 0.6s cubic-bezier(0.785, 0.135, 0.15, 0);
    transition: all 0.6s cubic-bezier(0.785, 0.135, 0.15, 0);
    position: absolute;
    right: -30px;
    opacity: 0;
  }
  > a:hover > i:nth-child(2) {
    right: -50px;
    opacity: 1;
  }
`;

const Info = styled.div`
  img {
    position: fixed;
    top: 20vh;
    left: 20vh;
    @media screen and (max-width: 992px) {
      top: 17vh;
      left: 5vh;
    }
  }
`;

const Hr = styled.hr`
  width: 340px;
  height: 20px;
  border: 0;
  background: #e3f2fd;
  position: absolute;
  top: 34vh;
  right: 0;
  left: 0;
  z-index: -1;
  animation-delay: 0.2s;
  @media screen and (max-width: 992px) {
    width: 250px;
    top: 29vh;
  }
`;

const Smile = styled.div`
  position: fixed;
  bottom: 25vh;
  right: 24%;
  font-size: 72px;
  animation-delay: 1.8s;
  span#leftEye {
    color: #969696;
  }
  span#rightEye {
    padding: 0 0 0 14px;
    color: #969696;
  }
  span#boca {
    transform: rotateZ(89deg);
    display: block;
    margin: -37px 0 1px 14px;
    color: #969696;
  }
  @media screen and (max-width: 992px) {
    bottom: 5vh;
    right: 24%;
  }
`;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animation: "fadeInUp",
    };
    this.goTo = this.goTo.bind(this);
  }
  goTo = (url) => {
    this.setState({
      animation: "fadeOut",
    });
    setTimeout(() => {
      this.props.history.push(url);
    }, 2000);
  };
  render() {
    return (
      <Wrapper>
        <Info>
          <BackButton
            animation={this.state.animation}
            onClick={() => this.goTo("/")}
          />
          <Title className={`animated ${this.state.animation}`}>
            {bo.contacts.title}
          </Title>
          <Hr className={`animated ${this.state.animation}`} />
          <List>
            <ListItem className={`animated ${this.state.animation}`}>
              <a
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/mariana-miguel-braga/"
                target="_blank"
              >
                <i class="fab fa-linkedin-in"></i>
                {bo.contacts.name}
                <i class="fas fa-external-link-square-alt"></i>
              </a>
            </ListItem>
            <ListItem className={`animated ${this.state.animation}`}>
              <a
                rel="noopener noreferrer"
                href="mailto:geral@marianabraga.com"
                target="_blank"
              >
                <i class="far fa-paper-plane"></i>
                {bo.contacts.email}
                <i class="fas fa-external-link-square-alt"></i>
              </a>
            </ListItem>
            <ListItem className={`animated ${this.state.animation}`}>
              <a rel="noopener noreferrer" href={cv} target="_blank">
                <i class="far fa-save"></i>
                {bo.contacts.cv}
                <i class="fas fa-external-link-square-alt"></i>
              </a>
            </ListItem>
            <ListItem className={`animated ${this.state.animation}`}>
              <a
                rel="noopener noreferrer"
                href="https://blog.marianabraga.com"
                target="_blank"
              >
                <i class="far fa-newspaper"></i>
                {bo.contacts.blog}
                <i class="fas fa-external-link-square-alt"></i>
              </a>
            </ListItem>
            <ListItem className={`animated ${this.state.animation}`}>
              <i class="fas fa-map-marker-alt"></i>
              {bo.contacts.local}
            </ListItem>
          </List>
        </Info>
        <Smile className={`animated ${this.state.animation}`}>
          <span id="leftEye">.</span>
          <span id="rightEye">.</span>
          <br />
          <span id="boca">)</span>
        </Smile>
      </Wrapper>
    );
  }
}

export default Home;
